#!/bin/bash
# This file is part of the SwagArch GNU/Linux distribution
# Copyright (c) 2016 Mike Krüger
# 
# Adapted by TGurl77 <tgurl@wadam.org>
#
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

function cleanup() {
    rm -Rrf -- */
}

function cloneall() {
    git clone https://gitlab.com/SwagArch/swagarch-meta.git
    git clone https://gitlab.com/SwagArch/swagarch-keyring.git
    git clone https://aur.archlinux.org/gnome-encfs-manager-bin.git
    git clone https://aur.archlinux.org/libgee06.git
    git clone https://aur.archlinux.org/pamac-aur.git
    git clone https://aur.archlinux.org/mkinitcpio-openswap.git
    git clone https://aur.archlinux.org/qgnomeplatform-git.git
    git clone https://aur.archlinux.org/ckbcomp.git 
    git clone https://gitlab.com/SwagArch/thunar-shares-plugin-swagarch
    git clone https://aur.archlinux.org/mugshot.git
    git clone https://gitlab.com/SwagArch/swagarch-mirrorlist.git
    git clone https://aur.archlinux.org/xfce4-panel-profiles.git
    git clone https://aur.archlinux.org/xed.git
    git clone https://aur.archlinux.org/hblox-git.git
    git clone https://aur.archlinux.org/update-grub.git
    git clone https://aur.archlinux.org/lightdm-gdmflexiserver.git
    git clone https://aur.archlinux.org/trizen.git
    git clone https://aur.archlinux.org/thunarx-python.git
    git clone https://aur.archlinux.org/pix.git
    git clone https://aur.archlinux.org/menulibre.git
    git clone https://gitlab.com/SwagArch/swagarch-lsb-release.git
    git clone https://aur.archlinux.org/redshift-light.git
    git clone https://aur.archlinux.org/yay-bin.git

    #Get calamares package
    get_package "calamares" \
    "https://gitlab.com/SwagArch/calamares-configs/raw/master/pkg/PKGBUILD" \
    ""

    #Get swagarch-wallpapers package
    get_package "swagarch-wallpapers" \
    "https://gitlab.com/SwagArch/swagarch-wallpapers/raw/master/PKGBUILD" \
    "https://gitlab.com/SwagArch/swagarch-wallpapers/raw/master/swagarch-wallpapers.install"

    #Get swagarch-base-skel package
    get_package "swagarch-base-skel" \
    "https://gitlab.com/SwagArch/desktop-settings/raw/master/pkg/PKGBUILD" \
    "https://gitlab.com/SwagArch/desktop-settings/raw/master/pkg/swagarch-base-skel.install"

    #Get light-locker-settings
    get_package "light-locker-settings" \
    "https://raw.githubusercontent.com/Antergos/antergos-packages/master/antergos/light-locker-settings/PKGBUILD" \
    ""

    #Get swagarch-settings-manager
    get_package "swagarch-settings-manager" \
    "https://gitlab.com/SwagArch/swagarch-settings-manager/raw/master/pkg/PKGBUILD" \
    ""

    #Get swagarch-artwork
    get_package "swagarch-artwork" \
    "https://gitlab.com/SwagArch/swagarch-artwork/raw/master/pkg/PKGBUILD" \
    ""

        #Get swagarch-icon-theme
    get_package "swagarch-icon-theme" \
    "https://gitlab.com/SwagArch/swagarch-icon-theme/raw/master/pkg/PKGBUILD" \
    ""

    #Get swagarch-config-dconf
    get_package "swagarch-config-dconf" \
    "https://gitlab.com/SwagArch/swagarch-config-dconf/raw/master/pkg/PKGBUILD" \
    "https://gitlab.com/SwagArch/swagarch-config-dconf/raw/master/pkg/swagarch-config-dconf.install"

    #Get swagarch-config-system
    get_package "swagarch-config-system" \
    "https://gitlab.com/SwagArch/swagarch-config-system/raw/master/pkg/PKGBUILD" \
    "https://gitlab.com/SwagArch/swagarch-config-system/raw/master/pkg/swagarch-config-system.install"
}

function get_package() {
    package=$1
    pkbuild_url=$2
    install_url=$3
    mkdir $package
    cd $package
    wget $pkbuild_url
    if [ -n "$install_url" ]; then
        wget $install_url
    fi
    cd ..
}

function delete_dotgit_dirs() {
    for dir in */ ;
    do
        dir=${dir%*/}
        if [ "$dir" == "." ] || [ "$dir" == ".." ]; then
            continue;
        fi
	    cd $dir
	    rm -rf .git
        echo "delete .git from folder "$dir
        cd ..
    done
}

cleanup
cloneall
delete_dotgit_dirs
